import { FeatureEmsModule } from '@microservices/feature/ems';
import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [FeatureEmsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
