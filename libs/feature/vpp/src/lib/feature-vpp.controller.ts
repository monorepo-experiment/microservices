import { SharedUserService } from '@microservices/shared/user';
import { Controller, Get, Query } from '@nestjs/common';

@Controller('vpp')
export class FeatureVppController {
  constructor(private userService: SharedUserService) {}

  @Get('hello')
  async hello(@Query('token') token: string): Promise<string> {
    if (!(await this.userService.validate(token))) {
      return 'invalid';
    }
    return 'world vpp';
  }
}
